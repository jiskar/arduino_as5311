// written by Jiskar Schmitz
// this sketch reads the position value from a AS5311 magnetic encoder chip and writes the postition to an lcd

#include <SPI.h>
#include <LcdBarGraph.h>
#include <Wire.h>
#include <LiquidCrystal.h>

const int backlightPin = 10;
const int indexPin = 12;
const int ledPin = 13;
const int CSpin = 11; //chip select
const int CLKpin = 2;
const int DIpin = 3;

byte lcdNumCols = 16; // -- number of columns in the LCD
int index = 0;
long absPos = 0;
int n=0;
int lastPos = 0;
int offset = 0;
int topOfScale = 966; //formatting option for the bargraph. The values correspond to the raw sensor value (0..1024). set to 1024 if you want full scale
int bottomOfScale = 886; //formatting option for the bargraph. The values correspond to the raw sensor value (0..1024). set to 0 if you want full scale

 LiquidCrystal lcd( 8, 9, 4, 5, 6, 7 );
// LcdBarGraph lbg(&lcd, lcdNumCols,0,1);  // -- creating bargraph instance

void setup(){
  pinMode(indexPin, INPUT);
  pinMode(ledPin, OUTPUT);
  pinMode(CSpin, OUTPUT);
  pinMode(DIpin, INPUT);
  pinMode(CLKpin, OUTPUT);
  digitalWrite(backlightPin, HIGH);
  digitalWrite(CLKpin, LOW);
  digitalWrite(CSpin, LOW);
  lcd.begin(16, 2);
  lcd.clear();
  lcd.print("hello, world");

  Serial.begin(9600);
  Serial.println('program start');
  offset = readPosition();
  }

void loop(){
  int newPos = readPosition();
  int diff = newPos - lastPos;
  long range = 4096L;
  
  if (diff > 1000){ //backwards zero crossing
    index -=1;
  }
  if (diff < -1000){ //forwards zero crossing
    index +=1;
  }
  lastPos=newPos;
    absPos = (range * index);
    absPos += lastPos;
//    absPos = absPos / 4096;
    int intPos = absPos / 65536;
//    Serial.write(intPos);
    Serial.println(absPos);
    
//    lbg.drawValue(intPos,topOfScale - bottomOfScale); //max scale = 1024 - the amount removed from the bottom part
    lcd.setCursor(0,1);
    lcd.print(absPos);
}

int readPosition(){
    int posi = 0;
     
    digitalWrite(CLKpin, HIGH);
    digitalWrite(CSpin, HIGH);
    delay(1);
    digitalWrite(CSpin, LOW);
    delay(1);
    digitalWrite(CLKpin, LOW);
    delay(1);
     
    for (int n = 0; n < 12; n++) {
          digitalWrite(CLKpin, HIGH);
          digitalWrite(CLKpin, LOW);
          posi = posi << 1;
          posi += digitalRead(DIpin);
       }

    digitalWrite(CSpin, HIGH);     
    
    return (posi);
}

String readStatus(){
     int magnitude = 0;
     int posi = 0;
     String str = "magnitude: ";
     //Serial.println('reading');
     digitalWrite(CLKpin, LOW); //clock low while you initiate data transfer gives status output
    
     digitalWrite(CSpin, LOW); //initiate data transfer
     delay(10); 
     for (int n = 0; n < 16; n++) {
            digitalWrite(CLKpin, HIGH);
            delay(2); 
            digitalWrite(CLKpin, LOW);
            magnitude = magnitude << 1;
            magnitude += digitalRead(DIpin);
            delay(2);
       }
     
     
     digitalWrite(CLKpin, HIGH);
     digitalWrite(CSpin, HIGH);
     delay(1);
     digitalWrite(CSpin, LOW);
     delay(10);
     digitalWrite(CLKpin, LOW);
     delay(2);
     
      for (int n = 0; n < 11; n++) {
      //Serial.println(n);
      digitalWrite(CLKpin, HIGH);
      delay(2); 
      digitalWrite(CLKpin, LOW);
      posi = posi << 1;
      posi += digitalRead(DIpin);
      delay(2);
   }
     
     digitalWrite(CSpin, HIGH);
    
     magnitude >> 4;

     str += magnitude;
     str += " position: ";
     str += posi;
     return(str);
}
